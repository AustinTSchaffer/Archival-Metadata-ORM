# Archival Metadata Python ORMs

Provides a Python-scripting interface for interacting with an Archivists'
Toolkit database, and other common archival-metadata database schemas, using SQL
Alchemy. This package also contains methods for converting those objects to
ArchivesSpace's JsonModel object format.

## Purpose

The purpose of this repository is to more efficiently, and more predictably,
migrate data from Archivists' Toolkit into ArchivesSpace, using the
ArchivesSpace API. Migrating AT objects to ArchivesSpace through an
ArchivesSpace Python ORM interface was considered, but has been shelved for now.
The purpose of this package is only to convert archival-metadata to
ArchivesSpace's JsonModel format. This package is not intended to enforce any of
ArchivesSpace's data validation functionality, meaning data is converted as-is,
with structure being considered only.

Archon DB support may be added as an additional package sometime in the future.
