from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='archival-metadata-orms',
    version='0.0.1',
    description='Provides methods and classes for pulling objects from common archival-metadata databases, converting them to plain dict objects, representing ArchivesSpace JsonModel object schemas.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://github.com/AustinTSchaffer/Archival-Metadata-ORM',
    author='Austin T Schaffer',
    author_email='schaffer.austin.t@gmail.com',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',

        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],

    keywords='archivesspace archives api archivists toolkit at archon',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=[
        'aspace-client',
        'mysql-connector-python>=8.0.12,<9',
        'SQLAlchemy>=1.2.12,<2',
    ],

    data_files=[
        ('', ['at_orm/jsonmodel_mappers/data/*.json'])
    ],

    package_data={},
    project_urls={
        'Bug Reports': 'https://github.com/AustinTSchaffer/Archival-Metadata-ORM/issues',
        'Feature Requests': 'https://github.com/AustinTSchaffer/Archival-Metadata-ORM/issues',
        'Source': 'https://github.com/AustinTSchaffer/Archival-Metadata-ORM',
    },
)
