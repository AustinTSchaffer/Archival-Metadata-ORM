import configparser
from typing import Union, Generic, Iterable

import sqlalchemy

from at_orm.enums import SupportedDatabaseDriver
import at_orm
from at_orm import orm
from at_orm import jsonmodel_mappers as mappers
from at_orm import mapped_objects


class ATMapper(object):
    """
    Defines methods for streaming data from AT as ArchivesSpace JSON Model
    objects. 
    """

    def __init__(self, dialect_driver: Union[str, SupportedDatabaseDriver],
                 db_host: str, db_name: str,
                 db_username: str, db_password: str):
        """
        Initialize an instance of the ArchivistsToolkitDataMapper using
        the specified DB connection settings.
        """
        dialect_driver = (
            dialect_driver.value
            if isinstance(dialect_driver, SupportedDatabaseDriver) else
            str(dialect_driver)
        )
        
        at_engine = sqlalchemy.create_engine(
            '{dialect_driver}://{db_username}:{db_password}@{db_host}/{db_name}'.format(
                dialect_driver=dialect_driver,
                db_username=db_username, db_password=db_password,
                db_host=db_host, db_name=db_name,
            )
        )

        self._session = sqlalchemy.orm.Session(bind=at_engine)

    @classmethod
    def init_from_config(cls, config: configparser.ConfigParser,
                         section='at_credentials', 
                         dialect_driver=SupportedDatabaseDriver.MYSQL,):
        """
        Initializes an instance of an ArchivistsToolkitDataMapper from an
        instance of the `configparser.ConfigParser` builtin Python config
        parser. The following keys must be set under the specified `section`
        of the specified `config`. If any are not set, the initialization will
        fail.

        Parameters:



        Config Keys:

            `"db_host"`: A url or IP address (plus port) that can be used to
            connect to the AT DB.

            `"db_name"`: The name of the AT database.

            `"db_username"`: The username of a db user that has read access to
            all of the data within the AT database.

            `"db_password"`: The password for that user.
        """

        dialect_driver = at_orm.enums.SupportedDatabaseDriver.MYSQL

        def at_credential(term, default=None):
            return config.get(section, term)

        _self = cls(
            dialect_driver=dialect_driver,
            db_host=at_credential('db_host'),
            db_name=at_credential('db_name'),
            db_username=at_credential('db_username'),
            db_password=at_credential('db_password'),
        )

        return _self

    @property
    def session(self):
        """
        Gets the sqlalchemy session, which can be used for custom queries.
        """
        return self._session

    def repositories(self) -> Iterable[at_orm.mapped_objects.MappedRepository]:
        r"""
        Streams repository JsonModels from the AT DB. Usage:

        ```
        for mapped_repo in mapper.repositories():
            client.post('/repositories', json=mapped_repo.repository)
            repo_uri = '/repositories/2' # Figure out the repo's actual URI
            agent_rep = client.get(
                client.get(repo_uri).json()['agent_representation']['ref']
            ).json()
            agent_rep.update(mapped_repo.agent_representation)
            client.post(agent_rep['uri'], json=agent_rep)
        ```
        """
        for repo in self.session.query(orm.Repositories): # type: orm.Repositories
            yield mapped_objects.MappedRepository(
                repository_id=repo.repositoryId,
                repository=mappers.repositories.repository(repo),
                agent_representation=mappers.repositories.contact_information(repo)
            )

    def users(self) -> Iterable[at_orm.mapped_objects.MappedUser]:
        """
        Streams user records from the AT DB as ArchivesSpace JsonModel
        objects.
        """
        for user in self.session.query(orm.Users).yield_per(1000): # type: orm.Users
            yield mapped_objects.MappedUser(
                user_id=user.userId,
                username=user.userName,
                user=mappers.users.user(user)
            )

    def subjects(self, vocab_uri: str = '/vocabulary/1') -> Iterable[at_orm.mapped_objects.MappedSubject]:
        """
        Streams subject records from the AT DB as ArchivesSpace JsonModel
        objects. Actual subject object is stored in the `subject` key of
        the dicts returned from this method.

        ```
        yield {
            'subject_id': -1 # old subject id from AT database
            'subject': {}
        }
        ```
        """

        for subject in self.session.query(orm.Subjects).yield_per(1000): # type: orm.Subjects
            yield mapped_objects.MappedSubject(
                subject_id=subject.subjectId,
                subject=mappers.subjects.subject(subject, vocab_uri)
            )

    def resources(self) -> Iterable[at_orm.mapped_objects.MappedResource]:
        """
        Streams resource trees from the AT DB as ArchivesSpace JsonModel
        objects. TODO: Structure of this endpoint.
        """

        for resource in self.session.query(orm.Resources).yield_per(1): # type: orm.Resources
            yield mapped_objects.MappedResource(
                resource_id=resource.resourceId,
                repository_id=resource.repositoryId,
                identifier=mappers.resources.resource_identifier(resource),
                resource=mappers.resources.resource(resource, self._session)
            )
