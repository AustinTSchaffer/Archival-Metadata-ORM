"""
Contains classes with named properties that encapsulate information that is
mapped from AT to ArchivesSpace. This is to more easily pull the archival
metadata from the output of the ArchivistsToolkitDataMapper, but also better
communicates any metadata about the archival metadata, that can be used to
link different components of the metadata together
"""


class MappedRepository(object):
    def __init__(self, repository_id: int, repository: dict,
                 agent_representation: dict):
        self.repository_id = repository_id
        self.repository = repository
        self.agent_representation = agent_representation

class MappedUser(object):
    def __init__(self, user_id: int, username: str, user: dict):
        self.user_id = user_id
        self.username = username
        self.user = user

class MappedSubject(object):
    def __init__(self, subject_id: int, subject: dict):
        self.subject_id = subject_id
        self.subject = subject

class MappedResource(object):
    def __init__(self, resource_id: int, repository_id: int, 
                 identifier: str, resource: dict):
        self.resource_id = resource_id
        self.repository_id = repository_id
        self.identifier = identifier
        self.resource = resource
