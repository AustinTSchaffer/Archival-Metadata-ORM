import enum


class SupportedDatabaseDriver(enum.Enum):
    """
    Specifies database drivers that have been tested with this package, and
    stores a reference to how the driver strings are formatted.
    """
    MYSQL = 'mysql+mysqlconnector'
