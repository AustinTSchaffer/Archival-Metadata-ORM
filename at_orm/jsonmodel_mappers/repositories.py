r"""
Provides methods for mapping an AT repository to an ArchivesSpace repository.
"""

from at_orm import orm
from at_orm.jsonmodel_mappers.agent_common import (
    telephone_business, telephone_fax
)


def repository(repo: orm.Repositories) -> dict:
    return {
        'repo_code': repo.shortName,
        'name': repo.repositoryName,
        'org_code': repo.agencyCode,
        'country': repo.countryCode,
        'parent_institution_name': repo.institutionName,
        'url': repo.url
    }

def contact_information(repo: orm.Repositories) -> dict:
    """
    Builds the 'agent_contacts' property of an agent record from the
    information stored in the Repositories record.
    ```
    return {
        'agent_contacts': [{}]
    }
    ```
    """
    return {
        'agent_contacts': [{
            'name': repo.institutionName,
            'address_1': repo.address1,
            'address_2': repo.address2,
            'address_3': repo.address3,
            'city': repo.city,
            'region': repo.region,
            'country': repo.country,
            'post_code': repo.mailCode,
            'telephones': (
                telephones(repo)
            ),
            'email': repo.email
        }]
    }

def telephones(repo: orm.Repositories) -> list:
    return (
        ([telephone_business(repo.telephone)] if repo.telephone else []) +
        ([telephone_fax(repo.fax)] if repo.fax else [])
    )
