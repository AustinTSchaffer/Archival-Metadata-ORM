import os
import json

class LanguageCodeMapper(object):
    """
    Maps the descriptive names of languages to their corresponding 
    """

    _language_map = {}

    @classmethod
    def get_language_map(cls) -> dict:
        """
        Returns the language map that is used to map the descriptive names of
        languages, to their corresponding ISO 639-2 language code.
        """
        if cls._language_map:
            return cls._language_map
        
        this_dir, _ = os.path.split(__file__)
        language_map_file = os.path.join(this_dir, 'data', 'language_map.json')

        with open(language_map_file) as lmf:
            cls._language_map = json.load(lmf)
        
        return cls._language_map

    @classmethod
    def get_language_code(cls, language: str) -> str:
        return cls.get_language_map()[language.strip().lower()]

def language_code(language_name: str) -> str:
    """
    Converts AT language codes to ASpace language codes.
    """
    return LanguageCodeMapper.get_language_code(language_name)
