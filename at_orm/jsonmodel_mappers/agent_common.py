def telephone_business(number: str, ext: str = '') -> dict:
    return telephone(number, number_type='business')

def telephone_fax(number: str, ext: str = '') -> dict:
    return telephone(number, number_type='fax')

def telephone(number: str, number_type: str = 'business',
              ext: str = '',) -> dict:
    return {
        'number': number,
        'ext': ext,
        'number_type': number_type,
    }
