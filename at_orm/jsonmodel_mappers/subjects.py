from aspace import jsonmodel

from at_orm import orm

def subject(subject: orm.Subjects, vocab_uri: str) -> dict:
    _subject = jsonmodel.subject(
        source=subject_source(subject.subjectSource),
        terms=subject_terms(subject, vocab_uri),
        vocabulary_uri=vocab_uri,
    )

    if subject.stringSubjectId:
        _subject.update(authority_id=subject.stringSubjectId)

    if subject.subjectScopeNote:
        _subject.update(scope_note=subject.subjectScopeNote)

    return _subject

def subject_source(source: str) -> str:
    _ss = source.lower()

    output = (
        'lcsh' if _ss == 'library of congress subject headings' else
        'local' if _ss == 'local sources' else 
        _ss
    )

    return output

def subject_terms(subject: orm.Subjects, vocabulary_uri: str) -> list:
    if subject.subjectTerm1 or subject.subjectTermType1:
        raise 'Unhandled subject configuration. Subject is multi-termed'

    terms = [
        jsonmodel.subject_term(
            term=term.strip(),
            term_type=subject_term_type(subject.subjectTermType),
            vocabulary_uri=vocabulary_uri
        )
        for term in
        subject.subjectTerm.split('--')
    ]

    return terms

def subject_term_type(term_type: str) -> str:
    _tt = term_type.lower()

    output = (
        'topical'       if '650' in _tt else
        'geographic'    if '651' in _tt else
        'occupation'    if '656' in _tt else
        'genre_form'    if '655' in _tt else
        'uniform_title' if '630' in _tt else
        'function'      if '657' in _tt else
        'temporal'      if '648' in _tt else
        'topical'
    )

    return output
