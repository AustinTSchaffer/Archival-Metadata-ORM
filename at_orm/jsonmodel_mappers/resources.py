import sqlalchemy

import aspace
from at_orm import orm, jsonmodel_mappers


def resource(resource: orm.Resources, session: sqlalchemy.orm.Session):
    """
    Left TODO: (Not counting archivalobjects)

    - eadIngestProblem
    - Notes
    - Linked creator agent / agents
    - Linked subjects
    """

    jsonmodel_resource = {
        'title': resource.title,
        'id_0': resource.resourceIdentifier1,
        'id_1': resource.resourceIdentifier2,
        'id_2': resource.resourceIdentifier3,
        'id_3': resource.resourceIdentifier4,
        'language': jsonmodel_mappers.enumerations.language_code(resource.languageCode),

        'level': aspace.util.convert_to_enumeration_value(resource.resourceLevel),
        'other_level': resource.otherLevel,
        'publish': not resource.internalOnly,
        'restrictions_apply': resource.restrictionsApply,
        'repository_processing_note': resource.repositoryProcessingNote,

        'dates': resource_dates(resource),
        'extents': resource_extents(resource),

        'ead_id': resource.eadFaUniqueIdentifier,
        'ead_location': resource.eadFaLocation,
        'finding_aid_title': resource.findingAidTitle,
        'finding_aid_subtitle': resource.findingAidSubtitle,
        'finding_aid_filing_title': resource.findingAidFilingTitle,
        'finding_aid_date': resource.findingAidDate,
        'finding_aid_author': resource.author,
        'finding_aid_description_rules': aspace.util.convert_to_enumeration_value(resource.descriptionRules),
        'finding_aid_language': resource.languageOfFindingAid,
        'finding_aid_sponsor': resource.sponsorNote,
        'finding_aid_edition_statement': resource.editionStatement,
        'finding_aid_series_statement': resource.series,
        'finding_aid_status': aspace.util.convert_to_enumeration_value(resource.findingAidStatus),
        'finding_aid_note': resource.findingAidNote,

        'revision_statements': revision_statements(resource),
    }

    if resource.userDefinedString1 or resource.userDefinedString2:
        jsonmodel_resource['user_defined'] = {
            'string_1': resource.userDefinedString1,
            'string_2': resource.userDefinedString2,
        }

    return jsonmodel_resource

def resource_identifier(resource: orm.Resources):
    resource_identifier = '-'.join([
        idcomp.strip() for idcomp in
        [
            resource.resourceIdentifier1,
            resource.resourceIdentifier2,
            resource.resourceIdentifier3,
            resource.resourceIdentifier4,
        ]
        if idcomp
    ])

    return resource_identifier

def revision_statements(resource: orm.Resources):
    if not resource.revisionDate and not resource.revisionDescription:
        return []

    return [{
        'date': resource.revisionDate or 'No Date',
        'description': resource.revisionDescription or 'No Description',
    }]

def resource_dates(resource: orm.Resources):
    dates = [{
        'expression': resource.dateExpression or 'No Expression',
        'date_type': 'inclusive',
        'label': 'creation',
        'begin': resource.dateBegin,
        'end': resource.dateEnd,
    }]

    if resource.bulkDateBegin or resource.bulkDateEnd:
        dates.append({
            'expression': resource.dateExpression or 'No Expression',
            'date_type': 'bulk',
            'label': 'creation',
            'begin': resource.bulkDateBegin,
            'end': resource.bulkDateEnd,
        })

    return dates

def resource_extents(resource: orm.Resources):
    extents = [{
        'portion': 'whole',
        'number': resource.extentNumber,
        'extent_type': aspace.util.convert_to_enumeration_value(resource.extentType),
        'container_summary': resource.containerSummary,
    }]

    return extents
