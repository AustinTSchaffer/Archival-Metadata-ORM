from at_orm import orm


def user(user: orm.Users):
    return {
        'username': user.userName,
        'name': user.fullName,
        'department': user.department,
        'email': user.email,
        'title': user.title,
        'is_admin': (
            user.accessClass and
            (int(user.accessClass) >= 5)
        )
    }
