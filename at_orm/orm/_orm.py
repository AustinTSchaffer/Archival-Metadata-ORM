# coding: utf-8
from sqlalchemy import Column, Date, DateTime, Float, ForeignKey, Index, String, Table, Text
from sqlalchemy.dialects.mysql import BIGINT, BIT, INTEGER
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Atplugindata(Base):
    __tablename__ = 'atplugindata'

    atPluginDataId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    dataString = Column(Text(collation='utf8_unicode_ci'), nullable=False)
    dataType = Column(String(100, 'utf8_unicode_ci'), nullable=False)
    dataName = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    dataVersion = Column(INTEGER(11))
    isObject = Column(BIT(1))
    pluginName = Column(String(255, 'utf8_unicode_ci'), nullable=False)


class Constants(Base):
    __tablename__ = 'constants'

    constantsId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    majorVersion = Column(String(10, 'utf8_unicode_ci'))
    minorVersion = Column(INTEGER(11), nullable=False)
    updateVersion = Column(INTEGER(11), nullable=False)
    defaultDateFormat = Column(String(255, 'utf8_unicode_ci'), nullable=False)


class Databasetables(Base):
    __tablename__ = 'databasetables'

    tableId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    tableName = Column(String(255, 'utf8_unicode_ci'), unique=True)
    className = Column(String(255, 'utf8_unicode_ci'), unique=True)


class Inlinetags(Base):
    __tablename__ = 'inlinetags'

    inLineTagId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    tagName = Column(String(255, 'utf8_unicode_ci'), nullable=False, unique=True)


t_instance_sequence = Table(
    'instance_sequence', metadata,
    Column('next_val', BIGINT(20))
)


class Lookuplist(Base):
    __tablename__ = 'lookuplist'

    lookupListId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    listName = Column(String(255, 'utf8_unicode_ci'), nullable=False, unique=True)
    listType = Column(INTEGER(11))
    pairedValues = Column(BIT(1))
    restrictToNmtoken = Column(BIT(1))


t_name_sequence = Table(
    'name_sequence', metadata,
    Column('next_val', BIGINT(20))
)


class Names(Base):
    __tablename__ = 'names'

    nameId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime, nullable=False)
    created = Column(DateTime, nullable=False)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    createdBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    nameType = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    sortName = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    createSortNameAutomatically = Column(BIT(1), nullable=False)
    nameNumber = Column(String(255, 'utf8_unicode_ci'))
    qualifier = Column(String(255, 'utf8_unicode_ci'))
    corporatePrimaryName = Column(String(255, 'utf8_unicode_ci'))
    corporateSubordinate1 = Column(String(255, 'utf8_unicode_ci'))
    corporateSubordinate2 = Column(String(255, 'utf8_unicode_ci'))
    personalPrimaryName = Column(String(255, 'utf8_unicode_ci'))
    personalRestOfName = Column(String(255, 'utf8_unicode_ci'))
    personalPrefix = Column(String(255, 'utf8_unicode_ci'))
    personalSuffix = Column(String(255, 'utf8_unicode_ci'))
    personalDates = Column(String(255, 'utf8_unicode_ci'))
    personalFullerForm = Column(String(255, 'utf8_unicode_ci'))
    personalTitle = Column(String(255, 'utf8_unicode_ci'))
    personalDirectOrder = Column(BIT(1))
    familyName = Column(String(255, 'utf8_unicode_ci'))
    familyNamePrefix = Column(String(255, 'utf8_unicode_ci'))
    nameSource = Column(String(50, 'utf8_unicode_ci'))
    nameRule = Column(String(50, 'utf8_unicode_ci'))
    descriptionType = Column(String(255, 'utf8_unicode_ci'))
    descriptionNote = Column(Text(collation='utf8_unicode_ci'))
    citation = Column(Text(collation='utf8_unicode_ci'))
    salutation = Column(String(255, 'utf8_unicode_ci'))
    contactAddress1 = Column(String(255, 'utf8_unicode_ci'))
    contactAddress2 = Column(String(255, 'utf8_unicode_ci'))
    contactCity = Column(String(255, 'utf8_unicode_ci'))
    contactRegion = Column(String(255, 'utf8_unicode_ci'))
    contactCountry = Column(String(255, 'utf8_unicode_ci'))
    contactMailCode = Column(String(255, 'utf8_unicode_ci'))
    contactPhone = Column(String(255, 'utf8_unicode_ci'))
    contactFax = Column(String(255, 'utf8_unicode_ci'))
    contactEmail = Column(String(255, 'utf8_unicode_ci'))
    contactName = Column(String(255, 'utf8_unicode_ci'))
    md5Hash = Column(String(255, 'utf8_unicode_ci'), unique=True)


class Notesetctypes(Base):
    __tablename__ = 'notesetctypes'

    notesEtcTypeId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    notesEtcName = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    notesEtcLabel = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    className = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    repeatingDataType = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    embeded = Column(BIT(1), nullable=False)
    allowsMultiPart = Column(BIT(1), nullable=False)
    includeInDigitalObjects = Column(BIT(1), nullable=False)


class Recordlocks(Base):
    __tablename__ = 'recordlocks'

    recordLockId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    className = Column(String(200, 'utf8_unicode_ci'), nullable=False)
    recordId = Column(BIGINT(20), nullable=False)
    userName = Column(String(100, 'utf8_unicode_ci'), nullable=False)
    hostIP = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    previousUpdateTime = Column(BIGINT(20), nullable=False)


t_repeating_data_sequence = Table(
    'repeating_data_sequence', metadata,
    Column('next_val', BIGINT(20))
)


class Repositories(Base):
    __tablename__ = 'repositories'

    repositoryId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    repositoryName = Column(String(255, 'utf8_unicode_ci'), nullable=False, unique=True)
    shortName = Column(String(255, 'utf8_unicode_ci'), nullable=False, unique=True)
    address1 = Column(String(255, 'utf8_unicode_ci'))
    address2 = Column(String(255, 'utf8_unicode_ci'))
    address3 = Column(String(255, 'utf8_unicode_ci'))
    city = Column(String(255, 'utf8_unicode_ci'))
    region = Column(String(255, 'utf8_unicode_ci'))
    country = Column(String(255, 'utf8_unicode_ci'))
    mailCode = Column(String(255, 'utf8_unicode_ci'))
    telephone = Column(String(255, 'utf8_unicode_ci'))
    fax = Column(String(255, 'utf8_unicode_ci'))
    email = Column(String(255, 'utf8_unicode_ci'))
    url = Column(String(255, 'utf8_unicode_ci'))
    countryCode = Column(String(255, 'utf8_unicode_ci'))
    agencyCode = Column(String(255, 'utf8_unicode_ci'))
    brandingDevice = Column(String(255, 'utf8_unicode_ci'))
    descriptiveLanguage = Column(String(255, 'utf8_unicode_ci'), index=True)
    ncesId = Column(String(20, 'utf8_unicode_ci'))
    institutionName = Column(String(255, 'utf8_unicode_ci'))


class Sessions(Base):
    __tablename__ = 'sessions'

    sessionId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    userName = Column(String(255, 'utf8_unicode_ci'))
    logonTimeStamp = Column(DateTime)


t_structured_data_sequence = Table(
    'structured_data_sequence', metadata,
    Column('next_val', BIGINT(20))
)


class Subjects(Base):
    __tablename__ = 'subjects'
    __table_args__ = (
        Index('UniqueSubjects', 'subjectTerm', 'subjectTermType', 'subjectSource', unique=True),
    )

    subjectId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    subjectTerm = Column(String(150, 'utf8_unicode_ci'), nullable=False)
    subjectTermType = Column(String(50, 'utf8_unicode_ci'), nullable=False)
    subjectSource = Column(String(100, 'utf8_unicode_ci'), nullable=False)
    subjectTerm1 = Column(String(150, 'utf8_unicode_ci'))
    subjectTermType1 = Column(String(50, 'utf8_unicode_ci'))
    subjectTerm2 = Column(String(150, 'utf8_unicode_ci'))
    subjectTermType2 = Column(String(50, 'utf8_unicode_ci'))
    subjectTerm3 = Column(String(150, 'utf8_unicode_ci'))
    subjectTermType3 = Column(String(50, 'utf8_unicode_ci'))
    subjectTerm4 = Column(String(150, 'utf8_unicode_ci'))
    subjectTermType4 = Column(String(50, 'utf8_unicode_ci'))
    subjectTerm5 = Column(String(150, 'utf8_unicode_ci'))
    subjectTermType5 = Column(String(50, 'utf8_unicode_ci'))
    subjectTerm6 = Column(String(150, 'utf8_unicode_ci'))
    subjectTermType6 = Column(String(50, 'utf8_unicode_ci'))
    displayForm = Column(Text(collation='utf8_unicode_ci'))
    stringSubjectId = Column(String(50, 'utf8_unicode_ci'))
    subjectScopeNote = Column(Text(collation='utf8_unicode_ci'))


class Accessions(Base):
    __tablename__ = 'accessions'
    __table_args__ = (
        Index('AccessionNumber', 'repositoryId', 'accessionNumber1', 'accessionNumber2', 'accessionNumber3', 'accessionNumber4', unique=True),
    )

    accessionId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime, nullable=False)
    created = Column(DateTime, nullable=False)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    createdBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    title = Column(Text(collation='utf8_unicode_ci'))
    dateExpression = Column(String(255, 'utf8_unicode_ci'))
    dateBegin = Column(INTEGER(11))
    dateEnd = Column(INTEGER(11))
    bulkDateBegin = Column(INTEGER(11))
    bulkDateEnd = Column(INTEGER(11))
    extentNumber = Column(Float(asdecimal=True))
    containerSummary = Column(Text(collation='utf8_unicode_ci'))
    extentType = Column(String(255, 'utf8_unicode_ci'))
    displayCreator = Column(String(255, 'utf8_unicode_ci'))
    displayRepository = Column(String(255, 'utf8_unicode_ci'))
    displaySource = Column(String(255, 'utf8_unicode_ci'))
    restrictionsApply = Column(BIT(1))
    accessionNumber1 = Column(String(10, 'utf8_unicode_ci'))
    accessionNumber2 = Column(String(10, 'utf8_unicode_ci'))
    accessionNumber3 = Column(String(10, 'utf8_unicode_ci'))
    accessionNumber4 = Column(String(10, 'utf8_unicode_ci'))
    accessionDate = Column(Date)
    resourceType = Column(String(255, 'utf8_unicode_ci'))
    description = Column(Text(collation='utf8_unicode_ci'))
    inventory = Column(Text(collation='utf8_unicode_ci'))
    conditionNote = Column(Text(collation='utf8_unicode_ci'))
    acquisitionType = Column(String(255, 'utf8_unicode_ci'))
    acknowledgementDate = Column(Date)
    agreementSentDate = Column(Date)
    agreementReceivedDate = Column(Date)
    processingPlan = Column(Text(collation='utf8_unicode_ci'))
    accessionProcessedDate = Column(Date)
    accessionDispositionNote = Column(Text(collation='utf8_unicode_ci'))
    retentionRule = Column(Text(collation='utf8_unicode_ci'))
    cataloged = Column(BIT(1))
    rightsTransferred = Column(BIT(1))
    catalogedNote = Column(Text(collation='utf8_unicode_ci'))
    accessRestrictionsNote = Column(Text(collation='utf8_unicode_ci'))
    useRestrictionsNote = Column(Text(collation='utf8_unicode_ci'))
    generalAccessionNote = Column(Text(collation='utf8_unicode_ci'))
    processingPriority = Column(String(255, 'utf8_unicode_ci'))
    processors = Column(String(255, 'utf8_unicode_ci'))
    processingStatus = Column(String(255, 'utf8_unicode_ci'))
    acknowledgementSent = Column(BIT(1))
    agreementSent = Column(BIT(1))
    agreementReceived = Column(BIT(1))
    accessionProcessed = Column(BIT(1))
    accessRestrictions = Column(BIT(1))
    useRestrictions = Column(BIT(1))
    catalogedDate = Column(Date)
    rightsTransferredDate = Column(Date)
    processingStartedDate = Column(Date)
    rightsTransferredNote = Column(Text(collation='utf8_unicode_ci'))
    userDefinedDate1 = Column(Date)
    userDefinedDate2 = Column(Date)
    userDefinedBoolean1 = Column(BIT(1))
    userDefinedBoolean2 = Column(BIT(1))
    userDefinedInteger1 = Column(INTEGER(11))
    userDefinedInteger2 = Column(INTEGER(11))
    userDefinedReal1 = Column(Float(asdecimal=True))
    userDefinedReal2 = Column(Float(asdecimal=True))
    userDefinedString1 = Column(String(255, 'utf8_unicode_ci'))
    userDefinedString2 = Column(String(255, 'utf8_unicode_ci'))
    userDefinedString3 = Column(String(255, 'utf8_unicode_ci'))
    userDefinedText1 = Column(Text(collation='utf8_unicode_ci'))
    userDefinedText2 = Column(Text(collation='utf8_unicode_ci'))
    userDefinedText3 = Column(Text(collation='utf8_unicode_ci'))
    userDefinedText4 = Column(Text(collation='utf8_unicode_ci'))
    repositoryId = Column(ForeignKey('repositories.repositoryId'), nullable=False, index=True)

    repositories = relationship('Repositories')


class Assessments(Base):
    __tablename__ = 'assessments'

    assessmentId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    conditionOfMaterial = Column(INTEGER(11))
    physicalAccess = Column(INTEGER(11))
    qualityOfHousing = Column(INTEGER(11))
    intellectualAccess = Column(INTEGER(11))
    interest = Column(INTEGER(11))
    documentationQuality = Column(INTEGER(11))
    researchValue = Column(INTEGER(11))
    userNumericalRating1 = Column(INTEGER(11))
    userNumericalRating2 = Column(INTEGER(11))
    generalNote = Column(Text(collation='utf8_unicode_ci'))
    conservationNote = Column(Text(collation='utf8_unicode_ci'))
    specialFormatNote = Column(Text(collation='utf8_unicode_ci'))
    architecturalMaterials = Column(BIT(1))
    artOriginals = Column(BIT(1))
    artifacts = Column(BIT(1))
    audioMaterials = Column(BIT(1))
    biologicalSpecimens = Column(BIT(1))
    botanicalSpecimens = Column(BIT(1))
    computerStorageUnits = Column(BIT(1))
    film = Column(BIT(1))
    glass = Column(BIT(1))
    photographs = Column(BIT(1))
    scrapbooks = Column(BIT(1))
    technicalDrawingsAndSchematics = Column(BIT(1))
    textiles = Column(BIT(1))
    vellumAndParchment = Column(BIT(1))
    videoMaterials = Column(BIT(1))
    other = Column(BIT(1))
    specialFormat1 = Column(BIT(1))
    specialFormat2 = Column(BIT(1))
    potentialMoldOrMoldDamage = Column(BIT(1))
    recentPestDamage = Column(BIT(1))
    deterioratingFilmBase = Column(BIT(1))
    specialConservationIssue1 = Column(BIT(1))
    specialConservationIssue2 = Column(BIT(1))
    specialConservationIssue3 = Column(BIT(1))
    brittlePaper = Column(BIT(1))
    metalFasteners = Column(BIT(1))
    newspaper = Column(BIT(1))
    tape = Column(BIT(1))
    thermofaxPaper = Column(BIT(1))
    otherConservationIssue1 = Column(BIT(1))
    otherConservationIssue2 = Column(BIT(1))
    otherConservationIssue3 = Column(BIT(1))
    exhibitionValueNote = Column(Text(collation='utf8_unicode_ci'))
    monetaryValue = Column(Float(asdecimal=True))
    monetaryValueNote = Column(Text(collation='utf8_unicode_ci'))
    estimatedProcessingTimePerFoot = Column(Float(asdecimal=True))
    totalExtent = Column(Float(asdecimal=True))
    totalEstimatedProcessingTime = Column(Float(asdecimal=True))
    whoDidSurvey = Column(Text(collation='utf8_unicode_ci'))
    amountOfTimeSurveyTook = Column(Float(asdecimal=True))
    userWhoCreatedRecord = Column(String(255, 'utf8_unicode_ci'))
    dateOfSurvey = Column(Date)
    reviewNeeded = Column(BIT(1))
    whoNeedsToReview = Column(Text(collation='utf8_unicode_ci'))
    reviewNote = Column(Text(collation='utf8_unicode_ci'))
    inactive = Column(BIT(1))
    repositoryId = Column(ForeignKey('repositories.repositoryId'), nullable=False, index=True)

    repositories = relationship('Repositories')


class Databasefields(Base):
    __tablename__ = 'databasefields'

    fieldId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    fieldName = Column(String(80, 'utf8_unicode_ci'), nullable=False)
    fieldLabel = Column(String(80, 'utf8_unicode_ci'))
    definition = Column(Text(collation='utf8_unicode_ci'))
    examples = Column(Text(collation='utf8_unicode_ci'))
    lookupList = Column(String(255, 'utf8_unicode_ci'))
    includeInSearchEditor = Column(BIT(1), nullable=False)
    excludeFromDefaultValues = Column(BIT(1), nullable=False)
    includeInRDE = Column(BIT(1), nullable=False)
    dataType = Column(String(80, 'utf8_unicode_ci'))
    stringLengthLimit = Column(INTEGER(11))
    returnScreenOrder = Column(INTEGER(11), nullable=False)
    tableId = Column(ForeignKey('databasetables.tableId'), index=True)

    databasetables = relationship('Databasetables')


class Inlinetagattributes(Base):
    __tablename__ = 'inlinetagattributes'

    inLineTagAttributeId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    valueList = Column(String(255, 'utf8_unicode_ci'))
    attributeName = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    inLineTagId = Column(ForeignKey('inlinetags.inLineTagId'), index=True)

    inlinetags = relationship('Inlinetags')


class Locationstable(Base):
    __tablename__ = 'locationstable'
    __table_args__ = (
        Index('UniqueLocations', 'building', 'floor', 'room', 'area', 'coordinate1Label', 'coordinate1NumericIndicator', 'coordinate1AlphaNumIndicator', 'coordinate2Label', 'coordinate2NumericIndicator', 'coordinate2AlphaNumIndicator', 'coordinate3Label', 'coordinate3NumericIndicator', 'coordinate3AlphaNumIndicator', 'classificationNumber', 'barcode', unique=True),
    )

    locationId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    building = Column(String(50, 'utf8_unicode_ci'))
    floor = Column(String(50, 'utf8_unicode_ci'))
    room = Column(String(50, 'utf8_unicode_ci'))
    area = Column(String(50, 'utf8_unicode_ci'))
    coordinate1Label = Column(String(50, 'utf8_unicode_ci'))
    coordinate1NumericIndicator = Column(Float(asdecimal=True))
    coordinate1AlphaNumIndicator = Column(String(20, 'utf8_unicode_ci'))
    coordinate2Label = Column(String(50, 'utf8_unicode_ci'))
    coordinate2NumericIndicator = Column(Float(asdecimal=True))
    coordinate2AlphaNumIndicator = Column(String(20, 'utf8_unicode_ci'))
    coordinate3Label = Column(String(50, 'utf8_unicode_ci'))
    coordinate3NumericIndicator = Column(Float(asdecimal=True))
    coordinate3AlphaNumIndicator = Column(String(20, 'utf8_unicode_ci'))
    classificationNumber = Column(String(50, 'utf8_unicode_ci'))
    barcode = Column(String(50, 'utf8_unicode_ci'))
    repositoryId = Column(ForeignKey('repositories.repositoryId'), nullable=False, index=True)

    repositories = relationship('Repositories')


class Lookuplistitems(Base):
    __tablename__ = 'lookuplistitems'

    lookupListItemId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    listItem = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    code = Column(String(255, 'utf8_unicode_ci'))
    editable = Column(BIT(1), nullable=False)
    atInitialValue = Column(BIT(1), nullable=False)
    lookupListId = Column(ForeignKey('lookuplist.lookupListId'), index=True)

    lookuplist = relationship('Lookuplist')


class Nonpreferrednames(Base):
    __tablename__ = 'nonpreferrednames'

    nameId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime, nullable=False)
    created = Column(DateTime, nullable=False)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    createdBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    nameType = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    sortName = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    createSortNameAutomatically = Column(BIT(1), nullable=False)
    nameNumber = Column(String(255, 'utf8_unicode_ci'))
    qualifier = Column(String(255, 'utf8_unicode_ci'))
    corporatePrimaryName = Column(String(255, 'utf8_unicode_ci'))
    corporateSubordinate1 = Column(String(255, 'utf8_unicode_ci'))
    corporateSubordinate2 = Column(String(255, 'utf8_unicode_ci'))
    personalPrimaryName = Column(String(255, 'utf8_unicode_ci'))
    personalRestOfName = Column(String(255, 'utf8_unicode_ci'))
    personalPrefix = Column(String(255, 'utf8_unicode_ci'))
    personalSuffix = Column(String(255, 'utf8_unicode_ci'))
    personalDates = Column(String(255, 'utf8_unicode_ci'))
    personalFullerForm = Column(String(255, 'utf8_unicode_ci'))
    personalTitle = Column(String(255, 'utf8_unicode_ci'))
    personalDirectOrder = Column(BIT(1))
    familyName = Column(String(255, 'utf8_unicode_ci'))
    familyNamePrefix = Column(String(255, 'utf8_unicode_ci'))
    primaryNameId = Column(ForeignKey('names.nameId'), nullable=False, index=True)

    names = relationship('Names')


class Repositorynotesdefaultvalues(Base):
    __tablename__ = 'repositorynotesdefaultvalues'

    repositoryNotesDefaultValueId = Column(BIGINT(20), primary_key=True)
    defaultTitle = Column(String(255, 'utf8_unicode_ci'))
    defaultContent = Column(Text(collation='utf8_unicode_ci'))
    repositoryId = Column(ForeignKey('repositories.repositoryId'), index=True)
    notesEtcTypeId = Column(ForeignKey('notesetctypes.notesEtcTypeId'), index=True)

    notesetctypes = relationship('Notesetctypes')
    repositories = relationship('Repositories')


class Repositorystatistics(Base):
    __tablename__ = 'repositorystatistics'

    RepositoryStatisticsId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    yearOfReport = Column(INTEGER(11))
    digitization = Column(BIT(1))
    exhibitLoans = Column(BIT(1))
    foodBeverage = Column(BIT(1))
    photographicReproduction = Column(BIT(1))
    retailGiftSales = Column(BIT(1))
    professionalFTE = Column(Float(asdecimal=True))
    nonProfessionalFTE = Column(Float(asdecimal=True))
    studentFTE = Column(Float(asdecimal=True))
    volunteerFTE = Column(Float(asdecimal=True))
    functDistAdmin = Column(Float(asdecimal=True))
    functDistProcessing = Column(Float(asdecimal=True))
    functDistPreservation = Column(Float(asdecimal=True))
    functDistReference = Column(Float(asdecimal=True))
    collFociHistorical = Column(BIT(1))
    collFociInstitutional = Column(BIT(1))
    collFociManuscript = Column(BIT(1))
    collFociPersonalPapers = Column(BIT(1))
    collFociOther = Column(BIT(1))
    majorSubjectAreas = Column(Text(collation='utf8_unicode_ci'))
    percentageOffSite = Column(Float(asdecimal=True))
    netUsableArea = Column(INTEGER(11))
    administrationOffices = Column(INTEGER(11))
    classrooms = Column(INTEGER(11))
    collectionsStorage = Column(INTEGER(11))
    readingRoom = Column(INTEGER(11))
    repositoryId = Column(ForeignKey('repositories.repositoryId'), index=True)

    repositories = relationship('Repositories')


class Resources(Base):
    __tablename__ = 'resources'
    __table_args__ = (
        Index('ResourceId', 'repositoryId', 'resourceIdentifier1', 'resourceIdentifier2', 'resourceIdentifier3', 'resourceIdentifier4', unique=True),
    )

    resourceId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime, nullable=False)
    created = Column(DateTime, nullable=False)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    createdBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    title = Column(Text(collation='utf8_unicode_ci'))
    dateExpression = Column(String(255, 'utf8_unicode_ci'))
    dateBegin = Column(INTEGER(11))
    dateEnd = Column(INTEGER(11))
    bulkDateBegin = Column(INTEGER(11))
    bulkDateEnd = Column(INTEGER(11))
    extentNumber = Column(Float(asdecimal=True))
    containerSummary = Column(Text(collation='utf8_unicode_ci'))
    extentType = Column(String(255, 'utf8_unicode_ci'))
    languageCode = Column(String(255, 'utf8_unicode_ci'), index=True)
    repositoryProcessingNote = Column(Text(collation='utf8_unicode_ci'))
    restrictionsApply = Column(BIT(1))
    displayCreator = Column(String(255, 'utf8_unicode_ci'))
    displayRepository = Column(String(255, 'utf8_unicode_ci'))
    displaySource = Column(String(255, 'utf8_unicode_ci'))
    internalOnly = Column(BIT(1), nullable=False)
    eadIngestProblem = Column(Text(collation='utf8_unicode_ci'))
    resourceIdentifier1 = Column(String(20, 'utf8_unicode_ci'))
    resourceIdentifier2 = Column(String(20, 'utf8_unicode_ci'))
    resourceIdentifier3 = Column(String(20, 'utf8_unicode_ci'))
    resourceIdentifier4 = Column(String(20, 'utf8_unicode_ci'))
    resourceLevel = Column(String(30, 'utf8_unicode_ci'))
    otherLevel = Column(String(30, 'utf8_unicode_ci'))
    sponsorNote = Column(Text(collation='utf8_unicode_ci'))
    eadFaUniqueIdentifier = Column(String(255, 'utf8_unicode_ci'))
    eadFaLocation = Column(String(255, 'utf8_unicode_ci'))
    findingAidTitle = Column(Text(collation='utf8_unicode_ci'))
    findingAidSubtitle = Column(Text(collation='utf8_unicode_ci'))
    findingAidFilingTitle = Column(Text(collation='utf8_unicode_ci'))
    findingAidDate = Column(String(255, 'utf8_unicode_ci'))
    author = Column(Text(collation='utf8_unicode_ci'))
    descriptionRules = Column(Text(collation='utf8_unicode_ci'))
    languageOfFindingAid = Column(Text(collation='utf8_unicode_ci'))
    editionStatement = Column(Text(collation='utf8_unicode_ci'))
    series = Column(Text(collation='utf8_unicode_ci'))
    revisionDate = Column(String(255, 'utf8_unicode_ci'))
    revisionDescription = Column(Text(collation='utf8_unicode_ci'))
    findingAidNote = Column(Text(collation='utf8_unicode_ci'))
    findingAidStatus = Column(String(255, 'utf8_unicode_ci'))
    nextPersistentId = Column(INTEGER(11))
    userDefinedString1 = Column(String(255, 'utf8_unicode_ci'))
    userDefinedString2 = Column(String(255, 'utf8_unicode_ci'))
    repositoryId = Column(ForeignKey('repositories.repositoryId'), nullable=False, index=True)

    repositories = relationship('Repositories')


class Simplerepeatablenotes(Base):
    __tablename__ = 'simplerepeatablenotes'

    simpleRepeatableNoteId = Column(BIGINT(20), primary_key=True)
    descriminator = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime, nullable=False)
    created = Column(DateTime, nullable=False)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    createdBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    label = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    sequenceNumber = Column(INTEGER(11), nullable=False)
    noteText = Column(Text(collation='utf8_unicode_ci'), nullable=False)
    nameId = Column(ForeignKey('names.nameId'), index=True)
    repositoryId = Column(ForeignKey('repositories.repositoryId'), index=True)

    names = relationship('Names')
    repositories = relationship('Repositories')


class Users(Base):
    __tablename__ = 'users'

    userId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    userName = Column(String(255, 'utf8_unicode_ci'), nullable=False, unique=True)
    fullName = Column(String(255, 'utf8_unicode_ci'))
    department = Column(String(255, 'utf8_unicode_ci'))
    email = Column(String(255, 'utf8_unicode_ci'))
    accessClass = Column(INTEGER(11))
    title = Column(String(255, 'utf8_unicode_ci'))
    repositoryId = Column(ForeignKey('repositories.repositoryId'), index=True)

    repositories = relationship('Repositories')


class Accessionslocations(Base):
    __tablename__ = 'accessionslocations'

    accessionsLocationsId = Column(BIGINT(20), primary_key=True)
    note = Column(Text(collation='utf8_unicode_ci'))
    accessionId = Column(ForeignKey('accessions.accessionId'), index=True)
    locationId = Column(ForeignKey('locationstable.locationId'), index=True)

    accessions = relationship('Accessions')
    locationstable = relationship('Locationstable')


class Accessionsresources(Base):
    __tablename__ = 'accessionsresources'
    __table_args__ = (
        Index('uniqueResourceAccessionLink', 'resourceId', 'accessionId', unique=True),
    )

    accessionsResourcesId = Column(BIGINT(20), primary_key=True)
    accessionId = Column(ForeignKey('accessions.accessionId'), index=True)
    resourceId = Column(ForeignKey('resources.resourceId'), index=True)

    accessions = relationship('Accessions')
    resources = relationship('Resources')


class Assessmentsaccessions(Base):
    __tablename__ = 'assessmentsaccessions'

    assessmentsAccessionsId = Column(BIGINT(20), primary_key=True)
    assessmentId = Column(ForeignKey('assessments.assessmentId'), index=True)
    accessionId = Column(ForeignKey('accessions.accessionId'), index=True)

    accessions = relationship('Accessions')
    assessments = relationship('Assessments')


class Assessmentsresources(Base):
    __tablename__ = 'assessmentsresources'

    assessmentsResourcesId = Column(BIGINT(20), primary_key=True)
    assessmentId = Column(ForeignKey('assessments.assessmentId'), index=True)
    resourceId = Column(ForeignKey('resources.resourceId'), index=True)

    assessments = relationship('Assessments')
    resources = relationship('Resources')


class Deaccessions(Base):
    __tablename__ = 'deaccessions'

    deaccessionsId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime, nullable=False)
    created = Column(DateTime, nullable=False)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    createdBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    description = Column(Text(collation='utf8_unicode_ci'), nullable=False)
    deaccessionDate = Column(Date)
    reason = Column(Text(collation='utf8_unicode_ci'))
    extent = Column(Float(asdecimal=True))
    extentType = Column(String(255, 'utf8_unicode_ci'))
    disposition = Column(String(255, 'utf8_unicode_ci'))
    notification = Column(BIT(1))
    resourceId = Column(ForeignKey('resources.resourceId'), index=True)
    accessionId = Column(ForeignKey('accessions.accessionId'), index=True)

    accessions = relationship('Accessions')
    resources = relationship('Resources')


class Defaultvalues(Base):
    __tablename__ = 'defaultvalues'
    __table_args__ = (
        Index('uniqueDefaultValue', 'repositoryId', 'fieldId', unique=True),
    )

    defaultValueId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    tableName = Column(String(255, 'utf8_unicode_ci'))
    stringValue = Column(String(255, 'utf8_unicode_ci'))
    textValue = Column(Text(collation='utf8_unicode_ci'))
    longValue = Column(BIGINT(20))
    doubleVlaue = Column(Float(asdecimal=True))
    intValue = Column(INTEGER(11))
    booleanValue = Column(BIT(1))
    dateValue = Column(Date)
    fieldId = Column(ForeignKey('databasefields.fieldId'), index=True)
    repositoryId = Column(ForeignKey('repositories.repositoryId'), index=True)

    databasefields = relationship('Databasefields')
    repositories = relationship('Repositories')


class Rdescreen(Base):
    __tablename__ = 'rdescreen'

    rdeScreenId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    rdeScreenName = Column(String(255, 'utf8_unicode_ci'), nullable=False, unique=True)
    className = Column(String(255, 'utf8_unicode_ci'))
    userId = Column(ForeignKey('users.userId'), index=True)

    users = relationship('Users')


class Resourcescomponents(Base):
    __tablename__ = 'resourcescomponents'

    resourceComponentId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime, nullable=False)
    created = Column(DateTime, nullable=False)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    createdBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    title = Column(Text(collation='utf8_unicode_ci'))
    dateExpression = Column(String(255, 'utf8_unicode_ci'))
    dateBegin = Column(INTEGER(11))
    dateEnd = Column(INTEGER(11))
    bulkDateBegin = Column(INTEGER(11))
    bulkDateEnd = Column(INTEGER(11))
    extentNumber = Column(Float(asdecimal=True))
    containerSummary = Column(Text(collation='utf8_unicode_ci'))
    extentType = Column(String(255, 'utf8_unicode_ci'))
    languageCode = Column(String(255, 'utf8_unicode_ci'), index=True)
    repositoryProcessingNote = Column(Text(collation='utf8_unicode_ci'))
    restrictionsApply = Column(BIT(1))
    internalOnly = Column(BIT(1))
    eadIngestProblem = Column(Text(collation='utf8_unicode_ci'))
    resourceLevel = Column(String(30, 'utf8_unicode_ci'))
    otherLevel = Column(String(30, 'utf8_unicode_ci'))
    sequenceNumber = Column(INTEGER(11))
    hasChild = Column(BIT(1), nullable=False)
    hasNotes = Column(BIT(1), nullable=False)
    subdivisionIdentifier = Column(String(255, 'utf8_unicode_ci'))
    persistentId = Column(String(10, 'utf8_unicode_ci'))
    parentResourceComponentId = Column(ForeignKey('resourcescomponents.resourceComponentId'), index=True)
    resourceId = Column(ForeignKey('resources.resourceId'), index=True)

    parent = relationship('Resourcescomponents', remote_side=[resourceComponentId])
    resources = relationship('Resources')


class Archdescphysicaldescriptions(Base):
    __tablename__ = 'archdescphysicaldescriptions'

    archDescPhysicalDescId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime, nullable=False)
    created = Column(DateTime, nullable=False)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    createdBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    extentType = Column(String(255, 'utf8_unicode_ci'))
    extentNumber = Column(Float(asdecimal=True))
    containerSummary = Column(Text(collation='utf8_unicode_ci'))
    physicalDetail = Column(Text(collation='utf8_unicode_ci'))
    dimensions = Column(Text(collation='utf8_unicode_ci'))
    resourceId = Column(ForeignKey('resources.resourceId'), index=True)
    accessionId = Column(ForeignKey('accessions.accessionId'), index=True)
    resourceComponentId = Column(ForeignKey('resourcescomponents.resourceComponentId'), index=True)

    accessions = relationship('Accessions')
    resourcescomponents = relationship('Resourcescomponents')
    resources = relationship('Resources')


class Archdescriptioninstances(Base):
    __tablename__ = 'archdescriptioninstances'

    archDescriptionInstancesId = Column(BIGINT(20), primary_key=True)
    instanceDescriminator = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    instanceType = Column(String(30, 'utf8_unicode_ci'), nullable=False)
    resourceComponentId = Column(ForeignKey('resourcescomponents.resourceComponentId'), index=True)
    resourceId = Column(ForeignKey('resources.resourceId'), index=True)
    container1NumericIndicator = Column(Float(asdecimal=True))
    container1AlphaNumIndicator = Column(String(255, 'utf8_unicode_ci'))
    container1Type = Column(String(20, 'utf8_unicode_ci'))
    container2NumericIndicator = Column(Float(asdecimal=True))
    container2AlphaNumIndicator = Column(String(255, 'utf8_unicode_ci'))
    container2Type = Column(String(20, 'utf8_unicode_ci'))
    container3NumericIndicator = Column(Float(asdecimal=True))
    container3AlphaNumIndicator = Column(String(255, 'utf8_unicode_ci'))
    container3Type = Column(String(20, 'utf8_unicode_ci'))
    containerLabel = Column(String(20, 'utf8_unicode_ci'))
    barcode = Column(String(80, 'utf8_unicode_ci'))
    locationId = Column(ForeignKey('locationstable.locationId'), index=True)
    userDefinedString1 = Column(String(255, 'utf8_unicode_ci'))
    userDefinedString2 = Column(String(255, 'utf8_unicode_ci'))
    userDefinedBoolean1 = Column(BIT(1))
    userDefinedBoolean2 = Column(BIT(1))
    parentResourceId = Column(ForeignKey('resources.resourceId'), index=True)

    locationstable = relationship('Locationstable')
    resources = relationship('Resources', primaryjoin='Archdescriptioninstances.parentResourceId == Resources.resourceId')
    resourcescomponents = relationship('Resourcescomponents')
    resources1 = relationship('Resources', primaryjoin='Archdescriptioninstances.resourceId == Resources.resourceId')


class Rdescreenpanels(Base):
    __tablename__ = 'rdescreenpanels'

    rdeScreenPanelId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    propertyName = Column(String(255, 'utf8_unicode_ci'))
    panelType = Column(String(255, 'utf8_unicode_ci'))
    sequenceNumber = Column(INTEGER(11), nullable=False)
    rdeScreenId = Column(ForeignKey('rdescreen.rdeScreenId'), index=True)

    rdescreen = relationship('Rdescreen')


class Digitalobjects(Base):
    __tablename__ = 'digitalobjects'

    digitalObjectId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    title = Column(Text(collation='utf8_unicode_ci'))
    dateExpression = Column(String(255, 'utf8_unicode_ci'))
    dateBegin = Column(INTEGER(11))
    dateEnd = Column(INTEGER(11))
    languageCode = Column(String(255, 'utf8_unicode_ci'), index=True)
    restrictionsApply = Column(BIT(1))
    eadDaoActuate = Column(String(255, 'utf8_unicode_ci'))
    eadDaoShow = Column(String(255, 'utf8_unicode_ci'))
    metsIdentifier = Column(String(255, 'utf8_unicode_ci'))
    objectType = Column(String(255, 'utf8_unicode_ci'))
    label = Column(String(255, 'utf8_unicode_ci'))
    objectOrder = Column(INTEGER(11))
    componentId = Column(String(255, 'utf8_unicode_ci'))
    parentDigitalObjectId = Column(ForeignKey('digitalobjects.digitalObjectId'), index=True)
    archDescriptionInstancesId = Column(ForeignKey('archdescriptioninstances.archDescriptionInstancesId'), index=True)
    repositoryId = Column(ForeignKey('repositories.repositoryId'), nullable=False, index=True)

    archdescriptioninstances = relationship('Archdescriptioninstances')
    parent = relationship('Digitalobjects', remote_side=[digitalObjectId])
    repositories = relationship('Repositories')


class Rdescreenpanelitems(Base):
    __tablename__ = 'rdescreenpanelitems'

    rdeScreenPanelItemId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    propertyName = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    sticky = Column(BIT(1))
    rdeScreenPanelId = Column(ForeignKey('rdescreenpanels.rdeScreenPanelId'), index=True)

    rdescreenpanels = relationship('Rdescreenpanels')


class Archdescriptiondates(Base):
    __tablename__ = 'archdescriptiondates'

    archDescriptionDatesId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime, nullable=False)
    created = Column(DateTime, nullable=False)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    createdBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    dateExpression = Column(String(255, 'utf8_unicode_ci'))
    dateBegin = Column(INTEGER(11))
    dateEnd = Column(INTEGER(11))
    isoDateBegin = Column(String(255, 'utf8_unicode_ci'))
    isoDateEnd = Column(String(255, 'utf8_unicode_ci'))
    isoDateBeginSeconds = Column(BIGINT(20))
    isoDateEndSeconds = Column(BIGINT(20))
    bulkDateBegin = Column(INTEGER(11))
    bulkDateEnd = Column(INTEGER(11))
    isoBulkDateBegin = Column(String(255, 'utf8_unicode_ci'))
    isoBulkDateEnd = Column(String(255, 'utf8_unicode_ci'))
    isoBulkDateBeginSeconds = Column(BIGINT(20))
    isoBulkDateEndSeconds = Column(BIGINT(20))
    certainty = Column(BIT(1))
    dateType = Column(String(255, 'utf8_unicode_ci'))
    era = Column(String(255, 'utf8_unicode_ci'))
    calendar = Column(String(255, 'utf8_unicode_ci'))
    resourceId = Column(ForeignKey('resources.resourceId'), index=True)
    accessionId = Column(ForeignKey('accessions.accessionId'), index=True)
    resourceComponentId = Column(ForeignKey('resourcescomponents.resourceComponentId'), index=True)
    digitalObjectId = Column(ForeignKey('digitalobjects.digitalObjectId'), index=True)

    accessions = relationship('Accessions')
    digitalobjects = relationship('Digitalobjects')
    resourcescomponents = relationship('Resourcescomponents')
    resources = relationship('Resources')


class Archdescriptionnames(Base):
    __tablename__ = 'archdescriptionnames'

    archDescriptionNamesId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    role = Column(String(255, 'utf8_unicode_ci'))
    nameLinkFunction = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    form = Column(String(255, 'utf8_unicode_ci'))
    primaryNameId = Column(ForeignKey('names.nameId'), nullable=False, index=True)
    resourceId = Column(ForeignKey('resources.resourceId'), index=True)
    resourceComponentId = Column(ForeignKey('resourcescomponents.resourceComponentId'), index=True)
    accessionId = Column(ForeignKey('accessions.accessionId'), index=True)
    digitalObjectId = Column(ForeignKey('digitalobjects.digitalObjectId'), index=True)

    accessions = relationship('Accessions')
    digitalobjects = relationship('Digitalobjects')
    names = relationship('Names')
    resourcescomponents = relationship('Resourcescomponents')
    resources = relationship('Resources')


class Archdescriptionrepeatingdata(Base):
    __tablename__ = 'archdescriptionrepeatingdata'

    archDescriptionRepeatingDataId = Column(BIGINT(20), primary_key=True)
    descriminator = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    repeatingDataType = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    title = Column(String(255, 'utf8_unicode_ci'))
    sequenceNumber = Column(INTEGER(11), nullable=False)
    eadIngestProblem = Column(Text(collation='utf8_unicode_ci'))
    resourceComponentId = Column(ForeignKey('resourcescomponents.resourceComponentId'), index=True)
    resourceId = Column(ForeignKey('resources.resourceId'), index=True)
    digitalObjectId = Column(ForeignKey('digitalobjects.digitalObjectId'), index=True)
    accessionId = Column(ForeignKey('accessions.accessionId'), index=True)
    noteContent = Column(Text(collation='utf8_unicode_ci'))
    internalOnly = Column(BIT(1))
    multiPart = Column(BIT(1))
    basic = Column(BIT(1))
    persistentId = Column(String(10, 'utf8_unicode_ci'))
    parentNotetId = Column(ForeignKey('archdescriptionrepeatingdata.archDescriptionRepeatingDataId'), index=True)
    notesEtcTypeId = Column(ForeignKey('notesetctypes.notesEtcTypeId'), index=True)
    note = Column(Text(collation='utf8_unicode_ci'))
    numeration = Column(String(255, 'utf8_unicode_ci'))
    href = Column(Text(collation='utf8_unicode_ci'))
    actuate = Column(String(255, 'utf8_unicode_ci'))
    showz = Column(String(255, 'utf8_unicode_ci'))

    accessions = relationship('Accessions')
    digitalobjects = relationship('Digitalobjects')
    notesetctypes = relationship('Notesetctypes')
    parent = relationship('Archdescriptionrepeatingdata', remote_side=[archDescriptionRepeatingDataId])
    resourcescomponents = relationship('Resourcescomponents')
    resources = relationship('Resources')


class Archdescriptionsubjects(Base):
    __tablename__ = 'archdescriptionsubjects'

    archDescriptionSubjectsId = Column(BIGINT(20), primary_key=True)
    resourceId = Column(ForeignKey('resources.resourceId'), index=True)
    resourceComponentId = Column(ForeignKey('resourcescomponents.resourceComponentId'), index=True)
    digitalObjectId = Column(ForeignKey('digitalobjects.digitalObjectId'), index=True)
    accessionId = Column(ForeignKey('accessions.accessionId'), index=True)
    subjectId = Column(ForeignKey('subjects.subjectId'), nullable=False, index=True)

    accessions = relationship('Accessions')
    digitalobjects = relationship('Digitalobjects')
    resourcescomponents = relationship('Resourcescomponents')
    resources = relationship('Resources')
    subjects = relationship('Subjects')


class Assessmentsdigitalobjects(Base):
    __tablename__ = 'assessmentsdigitalobjects'

    assessmentsDigitalObjectsId = Column(BIGINT(20), primary_key=True)
    assessmentId = Column(ForeignKey('assessments.assessmentId'), index=True)
    digitalObjectId = Column(ForeignKey('digitalobjects.digitalObjectId'), index=True)

    assessments = relationship('Assessments')
    digitalobjects = relationship('Digitalobjects')


class Fileversions(Base):
    __tablename__ = 'fileversions'

    fileVersionId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    uri = Column(Text(collation='utf8_unicode_ci'))
    useStatement = Column(Text(collation='utf8_unicode_ci'))
    sequenceNumber = Column(INTEGER(11), nullable=False)
    eadDaoActuate = Column(String(255, 'utf8_unicode_ci'))
    eadDaoShow = Column(String(255, 'utf8_unicode_ci'))
    digitalObjectId = Column(ForeignKey('digitalobjects.digitalObjectId'), index=True)

    digitalobjects = relationship('Digitalobjects')


class Bibitems(Base):
    __tablename__ = 'bibitems'

    archDescStructDataItemId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    sequenceNumber = Column(INTEGER(11), nullable=False)
    itemValue = Column(Text(collation='utf8_unicode_ci'), nullable=False)
    parentId = Column(ForeignKey('archdescriptionrepeatingdata.archDescriptionRepeatingDataId'), index=True)

    archdescriptionrepeatingdata = relationship('Archdescriptionrepeatingdata')


class Chronologyitems(Base):
    __tablename__ = 'chronologyitems'

    archDescStructDataItemId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    sequenceNumber = Column(INTEGER(11), nullable=False)
    eventDate = Column(Text(collation='utf8_unicode_ci'), nullable=False)
    parentId = Column(ForeignKey('archdescriptionrepeatingdata.archDescriptionRepeatingDataId'), index=True)

    archdescriptionrepeatingdata = relationship('Archdescriptionrepeatingdata')


class Indexitems(Base):
    __tablename__ = 'indexitems'

    archDescStructDataItemId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    sequenceNumber = Column(INTEGER(11), nullable=False)
    itemValue = Column(Text(collation='utf8_unicode_ci'), nullable=False)
    itemType = Column(Text(collation='utf8_unicode_ci'), nullable=False)
    reference = Column(Text(collation='utf8_unicode_ci'))
    referenceText = Column(String(255, 'utf8_unicode_ci'))
    parentId = Column(ForeignKey('archdescriptionrepeatingdata.archDescriptionRepeatingDataId'), index=True)

    archdescriptionrepeatingdata = relationship('Archdescriptionrepeatingdata')


class Listdefinitionitems(Base):
    __tablename__ = 'listdefinitionitems'

    archDescStructDataItemId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    sequenceNumber = Column(INTEGER(11), nullable=False)
    itemValue = Column(Text(collation='utf8_unicode_ci'), nullable=False)
    label = Column(Text(collation='utf8_unicode_ci'))
    parentId = Column(ForeignKey('archdescriptionrepeatingdata.archDescriptionRepeatingDataId'), index=True)

    archdescriptionrepeatingdata = relationship('Archdescriptionrepeatingdata')


class Listordereditems(Base):
    __tablename__ = 'listordereditems'

    archDescStructDataItemId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime)
    created = Column(DateTime)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'))
    createdBy = Column(String(255, 'utf8_unicode_ci'))
    sequenceNumber = Column(INTEGER(11), nullable=False)
    itemValue = Column(Text(collation='utf8_unicode_ci'), nullable=False)
    parentId = Column(ForeignKey('archdescriptionrepeatingdata.archDescriptionRepeatingDataId'), index=True)

    archdescriptionrepeatingdata = relationship('Archdescriptionrepeatingdata')


class Events(Base):
    __tablename__ = 'events'

    eventId = Column(BIGINT(20), primary_key=True)
    version = Column(BIGINT(20), nullable=False)
    lastUpdated = Column(DateTime, nullable=False)
    created = Column(DateTime, nullable=False)
    lastUpdatedBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    createdBy = Column(String(255, 'utf8_unicode_ci'), nullable=False)
    sequenceNumber = Column(INTEGER(11), nullable=False)
    eventDescription = Column(Text(collation='utf8_unicode_ci'))
    archDescStructDataItemId = Column(ForeignKey('chronologyitems.archDescStructDataItemId'), nullable=False, index=True)

    chronologyitems = relationship('Chronologyitems')
