from aspace import client, enums
from at_orm import mapper, orm

class BaseArchToolkitMigration(object):
    def __init__(self, atmapper: mapper.ATMapper, client: client.ASpaceClient):
        self.at_mapper = atmapper
        self.client = client

    def migrate_resource_enumerations(self):
        self.client.enum_management.update_enumeration(
            enums.Enumeration.EXTENT_EXTENT_TYPE,
            [
                _[0] for _ in self.at_mapper.session.query(
                    orm.Resources.extentType).distinct()
                    if _[0]
            ],
            cleanup_new_values=True,
            reorder_enumeration=True,
        )

        self.client.enum_management.update_enumeration(
            enums.Enumeration.RESOURCE_FINDING_AID_DESCRIPTION_RULES,
            [
                _[0] for _ in self.at_mapper.session.query(
                    orm.Resources.descriptionRules).distinct()
                    if _[0]
            ],
            cleanup_new_values=True,
            reorder_enumeration=True,
        )

        self.client.enum_management.update_enumeration(
            enums.Enumeration.RESOURCE_FINDING_AID_STATUS,
            [
                _[0] for _ in self.at_mapper.session.query(
                    orm.Resources.findingAidStatus).distinct()
                    if _[0]
            ],
            cleanup_new_values=True,
            reorder_enumeration=True,
        )

    def migrate_enumerations(self):
        self.migrate_resource_enumerations()

    def migrate(self):
        self.migrate_enumerations()
