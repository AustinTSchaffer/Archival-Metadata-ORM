import sqlalchemy
import sqlalchemy.orm

import at_orm

def main():
    # at_orm.build_py_classes.run('./at-models/', './at_orm/classes/')

    mapper = at_orm.data.ArchivistsToolkitDataMapper(
        dialect_driver=at_orm.drivers.SupportedDatabaseDriver.MYSQL,
        db_host='localhost:8006', db_name='at',
        username='root', password='123456'
    )

    for repo in mapper.users():
        print(repo)

    at_engine = sqlalchemy.create_engine('mysql+mysqlconnector://root:123456@localhost:8006/at')
    at_session = sqlalchemy.orm.Session(bind=at_engine)
    accessions = at_session.query(at_orm.orm.Accessions)
    adns = at_session.query(at_orm.orm.Archdescriptionnames)
    pass

if __name__ == '__main__':
    main()
